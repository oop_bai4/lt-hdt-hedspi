/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;


public class Employee {
    private String name;
    private String ID;
    private String age;
    private String address;
    
    public Employee()
    {}
    public Employee(String name,String ID,String age,String address)
    {
        this.name = name;
        this.ID = ID;
        this.age = age;
        this.address = address;
    }
    
    
    public void setAge(String age) {
        this.age = age;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }
    
    public String getName()
    {
        return name;
    }
    public String getID()
    {
        return ID;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public void setID(String ID)
    {
        this.ID = ID;
    }
}
