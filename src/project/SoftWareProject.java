/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

public class SoftWareProject {
    private String name;
    private String ID;
    private String start_time;
    private String end_time;
    public SoftWareProject()
    {}
    public SoftWareProject(String name,String ID,String start_time,String end_time)
    {
        this.ID = ID;
        this.name = name;
        this.start_time = start_time;
        this.end_time = end_time;
    }
    
    public void setID(String ID) {
        this.ID = ID;
    }

    public String getID() {
        return ID;
    }
    public String getName() {
        return name;
    }

    public String getStart_time() {
        return start_time;
    }

    public String getEnd_time() {
        return end_time;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
}
