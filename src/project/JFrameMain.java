package project;

import java.util.ArrayList;
public class JFrameMain extends javax.swing.JFrame {
    DatabaseMethod method = new DatabaseMethod();
    private ArrayList<Employee> arEmployee = new ArrayList<>();
    private ArrayList<SoftWareProject> arProject = new ArrayList<>();
    public JFrameMain() {
        initComponents();
        getAllNhanVien();
        getAllDuAn();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonNhanVien = new javax.swing.JButton();
        buttonThoat = new javax.swing.JButton();
        buttonDuAn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        buttonNhanVien.setBackground(new java.awt.Color(51, 204, 255));
        buttonNhanVien.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        buttonNhanVien.setText("QUẢN LÝ NHÂN VIÊN");
        buttonNhanVien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNhanVienActionPerformed(evt);
            }
        });

        buttonThoat.setBackground(new java.awt.Color(51, 204, 255));
        buttonThoat.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        buttonThoat.setText("THOÁT");
        buttonThoat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonThoatActionPerformed(evt);
            }
        });

        buttonDuAn.setBackground(new java.awt.Color(0, 204, 255));
        buttonDuAn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        buttonDuAn.setText("QUẢN LÝ DỰ ÁN");
        buttonDuAn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDuAnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buttonNhanVien, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                    .addComponent(buttonThoat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonDuAn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(50, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(buttonNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(buttonDuAn, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(buttonThoat, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonNhanVienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNhanVienActionPerformed
        this.dispose();
        new JFrameNhanVien(arEmployee).setVisible(true);
    }//GEN-LAST:event_buttonNhanVienActionPerformed

    private void buttonDuAnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDuAnActionPerformed
        this.dispose();
        new JFrameDuAn(arProject).setVisible(true);
    }//GEN-LAST:event_buttonDuAnActionPerformed

    private void buttonThoatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonThoatActionPerformed
        System.exit(0);
    }//GEN-LAST:event_buttonThoatActionPerformed

    private void getAllNhanVien()
    {
        ArrayList<String> temp;
        String col[] = {"ten","maso","tuoi","diachi"};
        temp = method.selectDb("select * from nhanvien", col);
        for(int i=0;i<temp.size()/4;i++)
            arEmployee.add(new Employee(temp.get(i*4).trim(),temp.get(i*4+1).trim(),
                    temp.get(i*4+2).trim(), temp.get(i*4+3).trim()));
    }
    private void getAllDuAn()
    {
        ArrayList<String> temp;
        String col[] = {"ten","id","batdau","ketthuc"};
        temp = method.selectDb("select * from duan", col);
        for(int i=0;i<temp.size()/4;i++)
            arProject.add(new SoftWareProject(temp.get(i*4).trim(),temp.get(i*4+1).trim(),
                    temp.get(i*4+2).trim(), temp.get(i*4+3).trim()));    
    }
    
    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameMain().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonDuAn;
    private javax.swing.JButton buttonNhanVien;
    private javax.swing.JButton buttonThoat;
    // End of variables declaration//GEN-END:variables
}
