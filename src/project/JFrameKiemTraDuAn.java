package project;

import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;

public class JFrameKiemTraDuAn extends javax.swing.JFrame {
    private JFrameNhanVien fParent;
    private int state;
    private ArrayList<SoftWareProject> arProjects = new ArrayList<>();
    public JFrameKiemTraDuAn(JFrameNhanVien fParent) {
        this.fParent = fParent;
        initComponents();
        this.setSize(800, 600);
        getAllProject();
        showList1();
        showJList(list2, arProjects);
        state = setState();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        list1 = new javax.swing.JList();
        jPanel3 = new javax.swing.JPanel();
        but_join = new javax.swing.JButton();
        but_drop = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        list2 = new javax.swing.JList();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridLayout(1, 1));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Kiểm Tra Dự Án"));
        jPanel1.setLayout(new java.awt.GridLayout(1, 3));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Dự Án Đang Thực Hiện");

        list1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                list1MousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(list1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.add(jPanel2);

        but_join.setText("Tham Gia Dự Án");
        but_join.setEnabled(false);
        but_join.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_joinActionPerformed(evt);
            }
        });

        but_drop.setText("Rút Khỏi Dự Án");
        but_drop.setEnabled(false);
        but_drop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_dropActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(but_drop, javax.swing.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
                    .addComponent(but_join, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(50, 50, 50))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(123, 123, 123)
                .addComponent(but_join, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addComponent(but_drop, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(128, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel3);
        jPanel3.getAccessibleContext().setAccessibleName("");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Các Dự Án Có Sẵn");

        list2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                list2MousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(list2);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.add(jPanel4);

        getContentPane().add(jPanel1);
        jPanel1.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        fParent.setVisible(true);
    }//GEN-LAST:event_formWindowClosed

    private void but_dropActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_dropActionPerformed
        String sql = "delete from thamgia where maso = "+fParent.arSearch.get(fParent.thutuNV).getID();
        System.out.println(sql);
        if(fParent.method.excuteDB(sql)==true)
        {
            arProjects.add(fParent.sproject);
            fParent.sproject = null;
            list1.removeAll();
            showList1();
            showJList(list2, arProjects);
            state = setState();
            JOptionPane.showMessageDialog(null, "Rời khỏi Dự Án thành công !");
            fParent.lb_duan.setText("--");
        }
        else
            JOptionPane.showMessageDialog(null, "Rời khỏi Dự Án không thành công !");
    }//GEN-LAST:event_but_dropActionPerformed

    private void list2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list2MousePressed
        if(state == 0)
            but_join.setEnabled(true);
        else 
            but_join.setEnabled(false);
        but_drop.setEnabled(false);
    }//GEN-LAST:event_list2MousePressed

    private void list1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list1MousePressed
        if(state > 0 )
            but_drop.setEnabled(true);
        but_join.setEnabled(false);        
    }//GEN-LAST:event_list1MousePressed

    private void but_joinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_joinActionPerformed
        int loc = list2.getSelectedIndex();
        String sql = "insert into thamgia (maso,id) values ("+fParent.arSearch.get(fParent.thutuNV).getID()+
                ","+arProjects.get(loc).getID()+")";
        System.out.println(sql);
        if(fParent.method.excuteDB(sql)==true)
        {
            fParent.sproject = arProjects.get(loc);
            arProjects.remove(fParent.sproject);
            but_drop.setEnabled(false);
            but_join.setEnabled(false);
            showList1();
            showJList(list2, arProjects);
            state = setState();
            fParent.lb_duan.setText(fParent.sproject.getName());
            JOptionPane.showMessageDialog(null, "Thêm vào Dự Án thành công !");
        }
        else
            JOptionPane.showMessageDialog(null, "Thêm vào Dự Án không thành công !");
    }//GEN-LAST:event_but_joinActionPerformed

    private void showList1()
    {
        list1.removeAll();
        DefaultListModel model = new DefaultListModel();
        model.removeAllElements();
        if(fParent.sproject!=null)
            model.addElement(fParent.sproject.getName());
        list1.setModel(model);
    }
    
    private void getAllProject()
    {
        ArrayList<String> temp;
        String col[] = {"ten","id","batdau","ketthuc"};
        String sql1="select * from duan";
        if(fParent.sproject==null)
            temp = fParent.method.selectDb(sql1, col);
        else
        {
            String sql2="select * from duan where id not in (select id from duan where id = "+
                    fParent.sproject.getID()+")";
            temp = fParent.method.selectDb(sql2, col);
        }
        for(int i=0;i<temp.size()/4;i++)
            arProjects.add(new SoftWareProject(temp.get(i*4).trim(),temp.get(i*4+1).trim(),
                    temp.get(i*4+2).trim(), temp.get(i*4+3).trim()));
    }
    
    private void showJList(JList list, ArrayList<SoftWareProject> array)
    {
        DefaultListModel lModel = new DefaultListModel();
        list.removeAll();
        for(SoftWareProject ar: array)
            lModel.addElement(ar.getName());
        list.setModel(lModel);
    }
    
    private int setState()
    {
        int i1 = list1.getModel().getSize();
        int i2 = list2.getModel().getSize();
        if(i2 == 0)
            if(i1==0) return -1;
            else return i1;
        return i1;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton but_drop;
    private javax.swing.JButton but_join;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList list1;
    private javax.swing.JList list2;
    // End of variables declaration//GEN-END:variables
}
