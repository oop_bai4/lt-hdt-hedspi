package project;

import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;

public class JFrameDuAn extends javax.swing.JFrame {
    public ArrayList<SoftWareProject> arProject;
    public ArrayList<SoftWareProject> arSearch = new ArrayList<>();
    public ArrayList<Employee> arEmployees = new ArrayList<>();
    public int ThuTuDA = -1;
    String memberSize;
    DatabaseMethod method = new DatabaseMethod();
    public JFrameDuAn(ArrayList<SoftWareProject> arProject) {
        this.arProject = arProject;
        initComponents();
        arSearch.addAll(arProject);
        showJList(ListDuAn, arSearch);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ListDuAn = new javax.swing.JList();
        jLabel2 = new javax.swing.JLabel();
        txt_tenduan = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        but_search = new javax.swing.JButton();
        txt_maduan = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        but_them = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        lb_name = new javax.swing.JLabel();
        lb_ID = new javax.swing.JLabel();
        lb_start = new javax.swing.JLabel();
        lb_end = new javax.swing.JLabel();
        lb_member = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        but_ten = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        but_ID = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        but_start = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        but_end = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        but_NV = new javax.swing.JButton();
        jPanel12 = new javax.swing.JPanel();
        but_xoa = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("DANH SÁCH DỰ ÁN");

        ListDuAn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ListDuAnMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(ListDuAn);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Tên Dự Án");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Mã");

        but_search.setText("Tìm");
        but_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_searchActionPerformed(evt);
            }
        });

        jPanel2.setLayout(new java.awt.GridLayout(6, 1));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Tên Dự Án");
        jPanel2.add(jLabel8);

        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("Mã Dự Án");
        jPanel2.add(jLabel15);

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Ngày bắt đầu");
        jPanel2.add(jLabel9);

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Ngày kết thúc");
        jPanel2.add(jLabel10);

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Số nhân viên");
        jPanel2.add(jLabel7);

        but_them.setText("Thêm Dự Án");
        but_them.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_themActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(but_them, javax.swing.GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(but_them, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        jPanel2.add(jPanel4);

        jPanel3.setLayout(new java.awt.GridLayout(6, 1));

        lb_name.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_name.setText("--");
        jPanel3.add(lb_name);

        lb_ID.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_ID.setText("--");
        jPanel3.add(lb_ID);

        lb_start.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_start.setText("--");
        jPanel3.add(lb_start);

        lb_end.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_end.setText("--");
        jPanel3.add(lb_end);

        lb_member.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_member.setText("--");
        jPanel3.add(lb_member);

        jPanel6.setLayout(new java.awt.GridLayout(6, 1));

        but_ten.setText("Chỉnh sửa");
        but_ten.setEnabled(false);
        but_ten.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_tenActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(but_ten, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(but_ten, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        jPanel6.add(jPanel7);

        but_ID.setText("Chỉnh sửa");
        but_ID.setEnabled(false);
        but_ID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_IDActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(but_ID, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(but_ID, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        jPanel6.add(jPanel8);

        but_start.setText("Chỉnh sửa");
        but_start.setEnabled(false);
        but_start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_startActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(but_start, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(but_start, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        jPanel6.add(jPanel9);

        but_end.setText("Chỉnh sửa");
        but_end.setEnabled(false);
        but_end.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_endActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(but_end, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(but_end, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        jPanel6.add(jPanel10);

        but_NV.setText("Danh sách nhân Viên");
        but_NV.setEnabled(false);
        but_NV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_NVActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(but_NV, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(but_NV, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        jPanel6.add(jPanel11);

        but_xoa.setText("Xóa Dự Án");
        but_xoa.setEnabled(false);
        but_xoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_xoaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(but_xoa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(but_xoa, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        jPanel6.add(jPanel12);

        jButton1.setText("Quay lại");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txt_tenduan, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txt_maduan)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(but_search))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txt_tenduan, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(but_search)
                        .addComponent(txt_maduan, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 549, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addContainerGap(91, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void but_tenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_tenActionPerformed
        String s = JOptionPane.showInputDialog(null, "Thay đổi tên Dự Án:");
        if (s!=null){
            String sql = "update duan set ten = '"+s+"' where id = "+arSearch.get(ThuTuDA).getID();        
            if(method.excuteDB(sql)==true)
            {
                lb_name.setText(s);
                arSearch.get(ThuTuDA).setName(s);
                showJList(ListDuAn, arSearch);
            }
            else
                JOptionPane.showMessageDialog(null, "Chỉnh sửa không thành công !");
        }
    }//GEN-LAST:event_but_tenActionPerformed

    private void but_themActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_themActionPerformed
        this.setVisible(false);
        new JFrameThemDuAn(this).setVisible(true);
    }//GEN-LAST:event_but_themActionPerformed

    private void but_IDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_IDActionPerformed
        String s = JOptionPane.showInputDialog(null, "Thay đổi Mã Dự Án:");
        if (s!=null){
            try {
                int i = Integer.parseInt(s);
                String sql = "update duan set id = "+s+" where id = "+arSearch.get(ThuTuDA).getID();
                if(method.excuteDB(sql)==true)
                {
                    lb_ID.setText(s);
                    arSearch.get(ThuTuDA).setID(s);
                }
            else
                JOptionPane.showMessageDialog(null, "Chỉnh sửa không thành công !");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Nhập không đúng !");
            }
        }
    }//GEN-LAST:event_but_IDActionPerformed

    private void but_startActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_startActionPerformed
        String s = JOptionPane.showInputDialog(null, "Sửa ngày thành lập Dự Án:");
        if (s!=null){
            String sql = "update duan set batdau = '"+s+"' where id = "+arSearch.get(ThuTuDA).getID();
            if(method.excuteDB(sql)==true)
            {
                lb_start.setText(s);
                arSearch.get(ThuTuDA).setStart_time(s);
            }
            else
                JOptionPane.showMessageDialog(null, "Chỉnh sửa không thành công !");
        }
    }//GEN-LAST:event_but_startActionPerformed

    private void but_endActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_endActionPerformed
        String s = JOptionPane.showInputDialog(null, "Sửa ngày kết thúc Dự Án:");
        if (s!=null){
            String sql = "update duan set ketthuc = '"+s+"' where id = "+arSearch.get(ThuTuDA).getID();
            if(method.excuteDB(sql)==true)
            {
                lb_end.setText(s);
                arSearch.get(ThuTuDA).setEnd_time(s);
            }
            else
                JOptionPane.showMessageDialog(null, "Chỉnh sửa không thành công !");
        }
    }//GEN-LAST:event_but_endActionPerformed

    private void but_NVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_NVActionPerformed
        this.setVisible(false);
        new JFrameKiemTraNhanVien(this).setVisible(true);
    }//GEN-LAST:event_but_NVActionPerformed

    private void but_xoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_xoaActionPerformed
        String sql = "DELETE FROM duan\n" +"WHERE id = "+arSearch.get(ThuTuDA).getID();
        String sql2 ="delete from thamgia\n"+"where id="+arSearch.get(ThuTuDA).getID();
        method.excuteDB(sql2);
        if(method.excuteDB(sql)==true)
        {
            arProject.remove(arSearch.get(ThuTuDA));
            arSearch.clear();
            arSearch.addAll(arProject);
            showJList(ListDuAn, arSearch);
            resetInfo();
            JOptionPane.showMessageDialog(null, "Xóa thành công !");
        }
        else
            JOptionPane.showMessageDialog(null, "Xóa không thành công !");
    }//GEN-LAST:event_but_xoaActionPerformed

    private void but_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_searchActionPerformed
        arSearch.clear();
        for(SoftWareProject temp : arProject)
            if(temp.getID().startsWith(txt_maduan.getText().trim())==true&&
                        temp.getName().startsWith(txt_tenduan.getText().trim())==true)
                arSearch.add(temp);
        if(arSearch.size()==0)
            JOptionPane.showMessageDialog(null, "Không tìm thấy kết quả nào!");
        showJList(ListDuAn, arSearch);
        resetInfo();
    }//GEN-LAST:event_but_searchActionPerformed

    private void ListDuAnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ListDuAnMouseClicked
        ThuTuDA = ListDuAn.getSelectedIndex();
        ArrayList<String> temp;
        String col[] = {"ten","maso","tuoi","diachi"};
        String sql = "select * from nhanvien natural join thamgia where id = "+arSearch.get(ThuTuDA).getID();
        arEmployees.clear();
        temp = method.selectDb(sql, col);
            for(int i=0;i<temp.size()/4;i++)
                arEmployees.add(new Employee(temp.get(i*4), temp.get(i*4+1), temp.get(i*4+2),
                        temp.get(i*4+3)));
        memberSize = ""+arEmployees.size();
        setStateButton(true);
        showInfo(arSearch, ThuTuDA);
    }//GEN-LAST:event_ListDuAnMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
        new JFrameMain().setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    public void showJList(JList list, ArrayList<SoftWareProject> array)
    {
        DefaultListModel lModel = new DefaultListModel();
        list.removeAll();
        for(SoftWareProject ar: array)
            lModel.addElement(ar.getName());
        list.setModel(lModel);
    }
    
    private void resetInfo()
    {
        ThuTuDA = -1;
        lb_ID.setText("--");
        lb_name.setText("--");
        lb_start.setText("--");
        lb_end.setText("--");
        lb_member.setText("--");
        setStateButton(false);
    }
    
    private void setStateButton(boolean state)
    {
        but_ten.setEnabled(state);
        but_start.setEnabled(state);
        but_ID.setEnabled(state);
        but_end.setEnabled(state);
        but_NV.setEnabled(state);
        but_xoa.setEnabled(state);
    }
    
    private void showInfo(ArrayList<SoftWareProject> em,int pos)
    {
        lb_ID.setText(em.get(pos).getID());
        lb_name.setText(em.get(pos).getName());
        lb_start.setText(em.get(pos).getStart_time());
        lb_end.setText(em.get(pos).getEnd_time());
        lb_member.setText(memberSize+"");
    }

    public static void main(String[] args) {
        ArrayList<String> ar;
        DatabaseMethod method = new DatabaseMethod();
        String col[] = {"Ten"};
        String sql="select * from nhanvien natural join thamgia where id = 5";
        ar = method.selectDb(sql, col);
        for(int i=0;i<ar.size();i++)
            System.out.println(ar.get(i));
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JList ListDuAn;
    private javax.swing.JButton but_ID;
    private javax.swing.JButton but_NV;
    private javax.swing.JButton but_end;
    private javax.swing.JButton but_search;
    private javax.swing.JButton but_start;
    private javax.swing.JButton but_ten;
    private javax.swing.JButton but_them;
    private javax.swing.JButton but_xoa;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lb_ID;
    private javax.swing.JLabel lb_end;
    public javax.swing.JLabel lb_member;
    private javax.swing.JLabel lb_name;
    private javax.swing.JLabel lb_start;
    private javax.swing.JTextField txt_maduan;
    private javax.swing.JTextField txt_tenduan;
    // End of variables declaration//GEN-END:variables
}
