package project;

import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class JFrameNhanVien extends javax.swing.JFrame {
    public int thutuNV = -1;
    public String tenduan;
    public String maduan;
    public SoftWareProject sproject ;
    public ArrayList<Employee> arEmployees;
    public ArrayList<Employee> arSearch = new ArrayList<>();
    DatabaseMethod method = new DatabaseMethod();
    public JFrameNhanVien(ArrayList<Employee> arEmployees) {
        this.arEmployees = arEmployees;
        initComponents();
        arSearch.addAll(this.arEmployees);
        showJList(ListEm, arSearch);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ListEm = new javax.swing.JList();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txt_nhanvien = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txt_ID = new javax.swing.JTextField();
        but_search = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jButton6 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        lb_ten = new javax.swing.JLabel();
        lb_tuoi = new javax.swing.JLabel();
        lb_ID = new javax.swing.JLabel();
        lb_diachi = new javax.swing.JLabel();
        lb_duan = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        but_ten = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        but_tuoi = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        but_ID = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        but_diachi = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        but_duan = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        but_xoa = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        ListEm.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        ListEm.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ListEmMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(ListEm);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("DANH SÁCH NHÂN VIÊN");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Tên Nhân Viên");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Mã");

        but_search.setText("Tìm");
        but_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_searchActionPerformed(evt);
            }
        });

        jPanel2.setLayout(new java.awt.GridLayout(6, 1));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Tên");
        jPanel2.add(jLabel4);

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Tuổi");
        jPanel2.add(jLabel5);

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Mã nhân viên");
        jPanel2.add(jLabel6);

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Địa chỉ");
        jLabel12.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel2.add(jLabel12);

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Dự án đi kèm");
        jPanel2.add(jLabel7);

        jButton6.setText("Thêm Nhân Viên");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.add(jPanel14);

        jPanel3.setLayout(new java.awt.GridLayout(6, 1));

        lb_ten.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_ten.setText("--");
        jPanel3.add(lb_ten);

        lb_tuoi.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_tuoi.setText("--");
        jPanel3.add(lb_tuoi);

        lb_ID.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_ID.setText("--");
        jPanel3.add(lb_ID);

        lb_diachi.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_diachi.setText("--");
        jPanel3.add(lb_diachi);

        lb_duan.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_duan.setText("--");
        lb_duan.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel3.add(lb_duan);

        jPanel4.setLayout(new java.awt.GridLayout(6, 1));

        but_ten.setText("Chỉnh Sửa");
        but_ten.setEnabled(false);
        but_ten.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_tenActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(but_ten, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addComponent(but_ten, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        jPanel4.add(jPanel5);

        but_tuoi.setText("Chỉnh Sửa");
        but_tuoi.setEnabled(false);
        but_tuoi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_tuoiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(but_tuoi, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(35, Short.MAX_VALUE)
                .addComponent(but_tuoi, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        jPanel4.add(jPanel6);

        but_ID.setText("Chỉnh Sửa");
        but_ID.setEnabled(false);
        but_ID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_IDActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(but_ID, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addComponent(but_ID, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        jPanel4.add(jPanel7);

        but_diachi.setText("Chỉnh Sửa");
        but_diachi.setEnabled(false);
        but_diachi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_diachiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(but_diachi, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(but_diachi, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
        );

        jPanel4.add(jPanel8);

        but_duan.setText("Xem dự án");
        but_duan.setEnabled(false);
        but_duan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_duanActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(but_duan, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addComponent(but_duan, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        jPanel4.add(jPanel9);

        but_xoa.setText("Xóa Nhân Viên");
        but_xoa.setEnabled(false);
        but_xoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_xoaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(but_xoa, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(but_xoa, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        jPanel4.add(jPanel10);

        jButton1.setText("Quay lại");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(15, 15, 15))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 7, Short.MAX_VALUE)
                                .addComponent(txt_nhanvien, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_ID))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(but_search, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(but_search, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
                    .addComponent(txt_ID)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt_nhanvien)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addGap(72, 72, 72))
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void but_diachiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_diachiActionPerformed
        String s = JOptionPane.showInputDialog(null, "Thay đổi địa chỉ nhân viên:");
        if (s!=null){
            String sql = "update nhanvien set diachi = '"+s+"' where maso = "+arSearch.get(thutuNV).getID();
            if(method.excuteDB(sql)==true)
            {
                lb_diachi.setText(s);
                arSearch.get(thutuNV).setAddress(s);
            }
            else
                JOptionPane.showMessageDialog(null, "Chỉnh sửa không thành công !");
        }
    }//GEN-LAST:event_but_diachiActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        this.setVisible(false);
        new JFrameThemNV(this).setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void ListEmMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ListEmMouseClicked
        thutuNV = ListEm.getSelectedIndex();
        ArrayList temp;
        String col[] = {"ten","id","batdau","ketthuc"};
        String sql = "select * from duan natural join thamgia "+ 
                "where maso = (select maso from nhanvien where maso = "+arSearch.get(thutuNV).getID()+")";
        temp = method.selectDb(sql, col);
        if(temp.size()==0) {tenduan = "--";sproject=null;}
        else{
            sproject = new SoftWareProject((String)temp.get(0), (String)temp.get(1), (String)temp.get(2),
                    (String)temp.get(3));
            tenduan = sproject.getName();
        }
        setStateButton(true);
        showInfo(arSearch, thutuNV);
    }//GEN-LAST:event_ListEmMouseClicked

    private void but_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_searchActionPerformed
        arSearch.clear();
        for(Employee temp : arEmployees)
            if(temp.getID().startsWith(txt_ID.getText().trim())==true&&
                        temp.getName().startsWith(txt_nhanvien.getText().trim())==true)
                arSearch.add(temp);
        if(arSearch.size()==0)
            JOptionPane.showMessageDialog(null, "Không tìm thấy kết quả nào!");
        showJList(ListEm, arSearch);
        resetInfo();
    }//GEN-LAST:event_but_searchActionPerformed

    private void but_tenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_tenActionPerformed
        String s = JOptionPane.showInputDialog(null, "Thay đổi tên nhân viên:");
        if (s!=null){
        String sql = "update nhanvien set ten = '"+s+"' where maso = "+arSearch.get(thutuNV).getID();
        if(method.excuteDB(sql)==true)
        {
            lb_ten.setText(s);
            arSearch.get(thutuNV).setName(s);
            showJList(ListEm, arSearch);
        }
        else
            JOptionPane.showMessageDialog(null, "Chỉnh sửa không thành công !");
        }
    }//GEN-LAST:event_but_tenActionPerformed

    private void but_duanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_duanActionPerformed
        this.setVisible(false);
        new JFrameKiemTraDuAn(this).setVisible(true);
    }//GEN-LAST:event_but_duanActionPerformed

    private void but_tuoiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_tuoiActionPerformed
        String s = JOptionPane.showInputDialog(null, "Thay đổi tuổi nhân viên:");
        if (s!=null){
            try {
                int i = Integer.parseInt(s);
                String sql = "update nhanvien set tuoi = "+s+" where maso = "+arSearch.get(thutuNV).getID();
                if(method.excuteDB(sql)==true)
                {
                    lb_tuoi.setText(s);
                    arSearch.get(thutuNV).setAge(s);
                }
            else
                JOptionPane.showMessageDialog(null, "Chỉnh sửa không thành công !");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Nhập không đúng !");
            }
        }
    }//GEN-LAST:event_but_tuoiActionPerformed

    private void but_IDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_IDActionPerformed
        String s = JOptionPane.showInputDialog(null, "Thay đổi mã Số nhân viên:");
        if (s!=null){
            try {
                int i = Integer.parseInt(s);
                String sql = "update nhanvien set maso = "+s+" where maso = "+arSearch.get(thutuNV).getID();
                if(method.excuteDB(sql)==true)
                {
                    lb_ID.setText(s);
                    arSearch.get(thutuNV).setID(s);
                }
            else
                JOptionPane.showMessageDialog(null, "Chỉnh sửa không thành công !");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Nhập không đúng !");
            }
        }
    }//GEN-LAST:event_but_IDActionPerformed

    private void but_xoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_xoaActionPerformed
        String sql = "DELETE FROM nhanvien\n" +"WHERE maso = "+arSearch.get(thutuNV).getID();
        String sql2 = "delete from thamgia\n" +"where maso = "+arSearch.get(thutuNV).getID();;
        method.excuteDB(sql2);
        if(method.excuteDB(sql)==true)
        {
            arEmployees.remove(arSearch.get(thutuNV));
            arSearch.clear();
            arSearch.addAll(arEmployees);
            showJList(ListEm, arSearch);
            resetInfo();
            JOptionPane.showMessageDialog(null, "Xóa thành công !");
        }
        else
            JOptionPane.showMessageDialog(null, "Xóa không thành công !");
    }//GEN-LAST:event_but_xoaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
        new JFrameMain().setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed
    
    public void showJList(JList list, ArrayList<Employee> array)
    {
        DefaultListModel lModel = new DefaultListModel();
        list.removeAll();
        for(Employee ar: array)
            lModel.addElement(ar.getName());
        list.setModel(lModel);
    }
    
    private void setStateButton(boolean state)
    {
        but_ten.setEnabled(state);
        but_tuoi.setEnabled(state);
        but_ID.setEnabled(state);
        but_diachi.setEnabled(state);
        but_duan.setEnabled(state);
        but_xoa.setEnabled(state);
    }
    private void showInfo(ArrayList<Employee> em,int pos)
    {
        lb_ID.setText(em.get(pos).getID());
        lb_ten.setText(em.get(pos).getName());
        lb_diachi.setText(em.get(pos).getAddress());
        lb_tuoi.setText(em.get(pos).getAge());
        lb_duan.setText(tenduan);
    }
    private void resetInfo()
    {
        thutuNV = -1;
        lb_ID.setText("--");
        lb_ten.setText("--");
        lb_diachi.setText("--");
        lb_tuoi.setText("--");
        lb_duan.setText("--");
        setStateButton(false);
    }
    
    static public boolean isTextEmpty(JTextField text)
    {
        if(text.getText().compareTo("")==0)
            return true;
        else
            return false;
    }
    
    public static void main(String[] args) {
        DatabaseMethod method = new DatabaseMethod();
        ArrayList<String> ar;
        String col[] = {"ten"};
        String sql = "select * from duan where id not in (select id from duan where id = 4)";
        ar = method.selectDb(sql, col);
        for(int i = 0 ; i<ar.size();i++)
            System.out.println(ar.get(i));
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JList ListEm;
    private javax.swing.JButton but_ID;
    private javax.swing.JButton but_diachi;
    private javax.swing.JButton but_duan;
    private javax.swing.JButton but_search;
    private javax.swing.JButton but_ten;
    private javax.swing.JButton but_tuoi;
    private javax.swing.JButton but_xoa;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lb_ID;
    private javax.swing.JLabel lb_diachi;
    public javax.swing.JLabel lb_duan;
    private javax.swing.JLabel lb_ten;
    private javax.swing.JLabel lb_tuoi;
    private javax.swing.JTextField txt_ID;
    private javax.swing.JTextField txt_nhanvien;
    // End of variables declaration//GEN-END:variables
}
