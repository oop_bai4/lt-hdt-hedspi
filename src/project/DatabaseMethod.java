/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DatabaseMethod {
    private final Connection con;
    private Statement state;
    private ResultSet resultSet;
    private PreparedStatement prstate;
    public DatabaseMethod()
    {
        con = ConnectDatabase.dbConnect();
    }
    boolean excuteDB(String sql) /* dùng hàm này để thực hiện những chức năng như delete or insert
            trả về true nếu thực hiện được else false*/
    {
        try {
            state = con.createStatement();
            state.execute(sql);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseMethod.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    ArrayList selectDb(String sql,String select[]) 
            /* Dùng hàm này để thực hiện chức năng select của database, sql là câu lệnh select
            select[] là mảng tên các trường cần select
            kết quả select được cho vào array theo thứ tự select[]*/
    {
        ArrayList array = new ArrayList();
        try {
            prstate = con.prepareStatement(sql);
            resultSet = prstate.executeQuery();
            while(resultSet.next())
            {
                for(int i = 0 ;i < select.length;i++)
                {
                    if(resultSet.getString(select[i])==null)
                        array.add("");
                    else
                        array.add(resultSet.getString(select[i]));
                }
            }
            return array;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseMethod.class.getName()).log(Level.SEVERE, null, ex);
            return array;
        }
    }
    public static void main(String[] args) {
        DatabaseMethod method = new DatabaseMethod();
        String sql = "select * from thamgia natural join nhanvien ";
        String cot[] = {"maso","id"};
        ArrayList ar = method.selectDb(sql, cot);
        for(int i=0;i<ar.size()/2;i++)
        {
            System.out.println(ar.get(i*2)+" "+ar.get(i*2+1));
        }
    }
}
