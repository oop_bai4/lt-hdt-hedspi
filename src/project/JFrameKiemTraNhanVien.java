package project;

import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;

public class JFrameKiemTraNhanVien extends javax.swing.JFrame {
    private JFrameDuAn fParent;
    private ArrayList<Employee> arEmployees = new ArrayList<>();
    public JFrameKiemTraNhanVien(JFrameDuAn fParent) {
        this.fParent = fParent;
        initComponents();
        this.setSize(800, 600);
        getAllNhanVien();
        showJList(list1, fParent.arEmployees);
        showJList(list2, arEmployees);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        list1 = new javax.swing.JList();
        jPanel3 = new javax.swing.JPanel();
        but_join = new javax.swing.JButton();
        but_drop = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        list2 = new javax.swing.JList();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridLayout(1, 1));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Kiểm Tra Nhân Viên"));
        jPanel1.setLayout(new java.awt.GridLayout(1, 3));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Các Nhân Viên Đang Tham Gia");

        list1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                list1MousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(list1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.add(jPanel2);

        but_join.setText("Thêm vào Dự Án");
        but_join.setEnabled(false);
        but_join.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_joinActionPerformed(evt);
            }
        });

        but_drop.setText("Loại ra Dự Án");
        but_drop.setEnabled(false);
        but_drop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_dropActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(but_join, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                    .addComponent(but_drop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(79, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(105, 105, 105)
                .addComponent(but_join, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(78, 78, 78)
                .addComponent(but_drop, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(122, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel3);

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Danh Sách Nhân Viên Chờ Dự Án");

        list2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                list2MousePressed(evt);
            }
        });
        jScrollPane3.setViewportView(list2);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.add(jPanel4);

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        fParent.setVisible(true);
    }//GEN-LAST:event_formWindowClosed

    private void list1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list1MousePressed
        if(list1.getModel().getSize()>0)
            but_drop.setEnabled(true);
        else 
            but_drop.setEnabled(false);
        but_join.setEnabled(false);
    }//GEN-LAST:event_list1MousePressed

    private void list2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list2MousePressed
        if(list2.getModel().getSize()>0)
            but_join.setEnabled(true);
        else
            but_join.setEnabled(false);
        but_drop.setEnabled(false);
    }//GEN-LAST:event_list2MousePressed

    private void but_joinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_joinActionPerformed
        int loc = list2.getSelectedIndex();
        String sql = "insert into thamgia (maso,id) values("+arEmployees.get(loc).getID()+","+
                fParent.arSearch.get(fParent.ThuTuDA).getID()+")";
        if(fParent.method.excuteDB(sql)==true)
        {
            fParent.arEmployees.add(arEmployees.get(loc));
            arEmployees.remove(loc);
            showJList(list1, fParent.arEmployees);
            showJList(list2, arEmployees);
            JOptionPane.showMessageDialog(null, "Join Dự Án thành công !");
            fParent.lb_member.setText(""+fParent.arEmployees.size());
            but_drop.setEnabled(false);
            but_join.setEnabled(false);
        }
        else
            JOptionPane.showMessageDialog(null, "Join Dự Án không thành công !");
    }//GEN-LAST:event_but_joinActionPerformed

    private void but_dropActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_dropActionPerformed
        int loc = list1.getSelectedIndex();
        String sql = "delete from thamgia where maso = "+fParent.arEmployees.get(loc).getID();
        System.out.println(sql);
        if(fParent.method.excuteDB(sql)==true)
        {
            arEmployees.add(fParent.arEmployees.get(loc));
            fParent.arEmployees.remove(loc);
            showJList(list1, fParent.arEmployees);
            showJList(list2, arEmployees);
            JOptionPane.showMessageDialog(null, "Drop khỏi Dự Án thành công !");
            fParent.lb_member.setText(""+fParent.arEmployees.size());
            but_drop.setEnabled(false);
            but_join.setEnabled(false);
        }
        else
            JOptionPane.showMessageDialog(null, "Drop khỏi Dự Án không thành công !");
    }//GEN-LAST:event_but_dropActionPerformed

    private void showJList(JList list, ArrayList<Employee> array)
    {
        DefaultListModel lModel = new DefaultListModel();
        list.removeAll();
        for(Employee ar: array)
            lModel.addElement(ar.getName());
        list.setModel(lModel);
    }
    
    private void getAllNhanVien()
    {
        ArrayList<String> temp;
        String col[] = {"ten","maso","tuoi","diachi"};
        String sql = "select * from nhanvien where maso not in (select maso from thamgia)";
        temp = fParent.method.selectDb(sql, col);
        for(int i=0;i<temp.size()/4;i++)
            arEmployees.add(new Employee(temp.get(i*4).trim(),temp.get(i*4+1).trim(),
                    temp.get(i*4+2).trim(), temp.get(i*4+3).trim()));
    }
    
    public static void main(String[] args) {
        DatabaseMethod method = new DatabaseMethod();
        ArrayList<String> ar; 
        String col[] = {"Ten"};
        String sql = "select * from nhanvien where maso not in (select maso from thamgia)";
        ar = method.selectDb(sql, col);
        for(int i = 0 ;i<ar.size();i++)
            System.out.println(ar.get(i));
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton but_drop;
    private javax.swing.JButton but_join;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JList list1;
    private javax.swing.JList list2;
    // End of variables declaration//GEN-END:variables
}
